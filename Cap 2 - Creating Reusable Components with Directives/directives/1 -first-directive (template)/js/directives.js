// essa diretiva poderá ser chamada atraves do atributo alert, exemplo: <div alert></div>
parking.directive("alert", function(){
	return {
		template: "<div class='alert'>" +
		"<span class='alert-topic'>" +
		 "Something went wrong!" +
		 "</span>" +
		 "<span class='alert alert-description'>" +
	 	 "You must inform the plate and the color of the car!" +
	 	 "</span>" +
	 	 "</div>"
	};
});