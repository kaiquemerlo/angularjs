parking.directive("alert", function(){
	return {
		restrict: 'E',
		scope:{
			topic: '@'
		},
		templateUrl: "alert.html",
		replace: true,
		transclude: true // o transclude exibe o conteudo que esta dentro da diretiva (podendo ser uma string ou outra diretiva)
	};
});