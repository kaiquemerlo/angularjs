parking.controller('parkingCtrl', function($scope){
	$scope.appTitle = "[Packt] Parking";
	$scope.showAlert = true;
	// alertTopic e description sao os valores passados nos atributos na chamada da diretiva
	$scope.alertTopic = "Something went wrong!";
	$scope.descriptionTopic = "You must inform the plate and the color of the car!";

	$scope.closeAlerts = function(){
		$scope.showAlert = false;
	};
});