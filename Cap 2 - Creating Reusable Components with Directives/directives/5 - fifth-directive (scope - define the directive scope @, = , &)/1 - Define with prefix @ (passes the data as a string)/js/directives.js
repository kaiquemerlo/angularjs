parking.directive("alert", function(){
	return {
		restrict: 'E', //A,C,M / AC / ACM / AM
		scope: {
			topic: '@topic', // o elemento topic esta pegando o conteudo do ATRIBUTO topic passado na chamada da diretiva
			description: '@description' // o elemento topic esta pegando o conteudo do ATRIBUTO description passado na chamada da diretiva
		},
		templateUrl: 'alert.html',
		replace: true
	};
});