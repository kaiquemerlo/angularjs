parking.directive("alert", function(){
	return {
		restrict: 'E', //A,C,M / AC / ACM / AM
		scope: {
			topic: '=topic', // o elemento topic esta pegando o conteudo do ATRIBUTO topic passado na chamada da diretiva que é definido no escopo do controller
			description: '=description', // o elemento topic esta pegando o conteudo do ATRIBUTO description passado na chamada da diretiva que é definido no escopo do controller
			close: '&close' // ta pegando o atributo close e executando no controller a funcao que esta sendo declarada dentro do atributo
		},
		templateUrl: 'alert.html',
		replace: true
	};
});