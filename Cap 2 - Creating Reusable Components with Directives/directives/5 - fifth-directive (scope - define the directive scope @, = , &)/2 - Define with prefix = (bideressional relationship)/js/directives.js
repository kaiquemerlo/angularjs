parking.directive("alert", function(){
	return {
		restrict: 'E', //A,C,M / AC / ACM / AM
		scope: {
			// (=) cria uma relação com o que está dentro do atributo definido na tag da diretiva e o que esta definido no scopo do controller com o valor desse atributo. 
			//ex: index.html <alert topic="var"></alert> 
			// controllers.js $scope.var = "ESSE VALOR SUBSTITUI O VALOR ATUAL DE topic"

			topic: '=topic', // o elemento topic esta pegando o conteudo do ATRIBUTO topic passado na chamada da diretiva que é definido no escopo do controller
			description: '=description' // o elemento topic esta pegando o conteudo do ATRIBUTO description passado na chamada da diretiva que é definido no escopo do controller
		},
		templateUrl: 'alert.html',
		replace: true
	};
});