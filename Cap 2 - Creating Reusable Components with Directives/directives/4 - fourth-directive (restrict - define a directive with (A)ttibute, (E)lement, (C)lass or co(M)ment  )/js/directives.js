parking.directive("alert", function(){
	return {
		restrict: 'E', //A,C,M / AC / ACM / AM
		templateUrl: "alert.html",
		replace: true
	};
});